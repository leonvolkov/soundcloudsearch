import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundCloudSearchComponent } from './sound-cloud-search.component';

describe('SoundCloudSearchComponent', () => {
  let component: SoundCloudSearchComponent;
  let fixture: ComponentFixture<SoundCloudSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoundCloudSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundCloudSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
