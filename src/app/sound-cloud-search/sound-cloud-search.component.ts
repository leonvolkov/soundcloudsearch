import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';

import { SoundCloudSearchService } from 'src/app/sound-cloud-search/sound-cloud-search.service';
import { SoundCloudSearchResults } from 'src/app/sound-cloud-search/sound-cloud-search-results';

@Component({
  selector: 'app-sound-cloud-search',
  templateUrl: './sound-cloud-search.component.html',
  styleUrls: ['./sound-cloud-search.component.scss']
})
export class SoundCloudSearchComponent implements OnInit {
  @Input() limit: number;
  private subscriptions = new Subscription();
  public next_url: string = null;
  public searchResults: Array<{}> = [];
  public keyword: string;
  public noResults = false;
  constructor(
    private soundCloudSearchService: SoundCloudSearchService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }


  onSearchButtonClick() {
    this.subscriptions.add(this.soundCloudSearchService.getTracksPageable(this.keyword, this.limit)
      .subscribe(this.onSearchResultsLoad.bind(this)));
  }

  onNextButtonClick() {
    this.subscriptions.add(this.soundCloudSearchService.getNextPage(this.next_url)
      .subscribe(this.onSearchResultsLoad.bind(this)));
  }

  onSearchResultsLoad(results: SoundCloudSearchResults) {
    this.searchResults = results.collection || [];
    this.next_url = results.next_href || null;
    this.noResults = this.searchResults.length === 0;
  }

}
