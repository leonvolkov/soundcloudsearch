export interface SoundCloudSearchResults {
  collection?: any;
  next_href?: string;
}
