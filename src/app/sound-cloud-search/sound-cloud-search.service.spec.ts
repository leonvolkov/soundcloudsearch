import { TestBed } from '@angular/core/testing';

import { SoundCloudSearchService } from './sound-cloud-search.service';

describe('SoundCloudSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoundCloudSearchService = TestBed.get(SoundCloudSearchService);
    expect(service).toBeTruthy();
  });
});
