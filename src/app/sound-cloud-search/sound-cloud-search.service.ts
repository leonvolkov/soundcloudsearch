import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_SC_URL =  'https://api.soundcloud.com/';
const CLIENT_ID = 'e59d8b005900e38649c1882b87cd828d';

@Injectable({
  providedIn: 'root'
})
export class SoundCloudSearchService {

  constructor(private http: HttpClient) { }

  getTracksPageable(keyword: string, limit: number) {
    const search_context = 'tracks';
    const query_str = `linked_partitioning=1&client_id=${CLIENT_ID}&q=${keyword}&limit=${limit}`;
    const url = `${BASE_SC_URL}${search_context}?${query_str}`;

    return this.http.get(url);
  }

  getNextPage(next_url: string) {
    return this.http.get(next_url);
  }

}
